# Tips and Tricks for Using R and Organizing Data

This project contains slides and a script I presented to a number of students of the Wildlife Graduate Student Association at the University of Florida on 03/29/2019.

## Slides
The slides are not my own, but borrowed from / shared by NEON (National Ecological Observatory Network), Naupaka Zimmerman (University of San Francisco) and Jenny Bryan (University of British Columbia / RStudio). I am grateful for their commitment to open science and efforts on promoting reproducibility.
The first presentation is available online:
1) http://neonscience.github.io/slide-shows/intro-reprod-science.html
2) The second presentation is a pdf on good naming conventions
3) The last presentation is a few slides about the "tidyverse" packages

## Script
There is a short R script included in this presentation/workshop that showcases some tidyverse functionalities to clean up data, and make a plot.
The data used is 30-minute precipitation data from a NEON site in Florida, the Ordway-Swisher Biological Station (https://www.neonscience.org/ and https://data.neonscience.org/home), "PRIPRE_30min.csv". 
There is also a file with data from NOAA, from the Gainesville Airport station (https://www.ncdc.noaa.gov/cdo-web/), "NOAA_data.csv", used to fill gaps in the NEON data (as an example).

### Additional
The binder example I showed during the presentation: https://elifesciences.org/labs/8653a61d/introducing-binder-2-0-share-your-interactive-research-environment 

Article "Best Practices for Scientific Computing": https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1001745

Article "Good enough practices in scientific computing": https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1001745